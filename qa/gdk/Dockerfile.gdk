FROM registry.gitlab.com/gitlab-org/gitlab-development-kit/asdf-bootstrapped-verify:main@sha256:af7e6e7a9d6338ca7045e878b9717a1c8feb507dbd1e69db7ef407af4074f27d

ENV GITLAB_LICENSE_MODE=test \
    GDK_KILL_CONFIRM=true

# Allow passwordless /etc/hosts update by gdk user
USER root
RUN echo "gdk ALL=(ALL) NOPASSWD: /usr/bin/tee -a /etc/hosts" >> /etc/sudoers

USER gdk

# Clone GDK at specific sha and bootstrap packages
#
ARG GDK_SHA=747ab64be815f5c239d5d63209527a42bd838e83
ARG GEM_CACHE=/home/gdk/.asdf/installs/ruby/3.1.4/lib/ruby/gems/3.1.0/cache
RUN --mount=type=cache,target=${GEM_CACHE},uid=1000,gid=1000 \
    set -eux; \
    git clone --depth 1 https://gitlab.com/gitlab-org/gitlab-development-kit.git && cd gitlab-development-kit; \
    git fetch --depth 1 origin ${GDK_SHA} && git -c advice.detachedHead=false checkout ${GDK_SHA}; \
    mkdir gitlab && make bootstrap

WORKDIR /home/gdk/gitlab-development-kit

COPY --chown=gdk:gdk qa/gdk/gdk.yml ./

# Build gitlab-shell
#
COPY --chown=gdk:gdk GITLAB_SHELL_VERSION ./gitlab/
RUN --mount=type=cache,target=${GEM_CACHE},uid=1000,gid=1000 \
    set -eux; \
    make gitlab-shell-setup \
    && cd gitlab-shell && go clean -cache -modcache -r

# Build gitlab-workhorse
#
COPY --chown=gdk:gdk VERSION GITLAB_WORKHORSE_VERSION ./gitlab/
COPY --chown=gdk:gdk workhorse ./gitlab/workhorse
RUN --mount=type=cache,target=${GEM_CACHE},uid=1000,gid=1000 \
    set -eux; \
    make gitlab-workhorse-setup && mv gitlab/workhorse ./ \
    && cd workhorse && go clean -cache -modcache -r

# Build gitaly
#
COPY --chown=gdk:gdk GITALY_SERVER_VERSION ./gitlab/
RUN --mount=type=cache,target=${GEM_CACHE},uid=1000,gid=1000 \
    set -eux; \
    make gitaly-setup; \
    cd gitaly \
    && go clean -cache -modcache -r \
    && rm -rf _build/cache \
              _build/deps/git/source \
              _build/deps/libgit2/source \
              _build/deps \
              _build/intermediate

# Install gitlab gem dependencies
#
COPY --chown=gdk:gdk Gemfile Gemfile.lock ./gitlab/
COPY --chown=gdk:gdk vendor/gems/ ./gitlab/vendor/gems/
COPY --chown=gdk:gdk gems/ ./gitlab/gems/
RUN --mount=type=cache,target=${GEM_CACHE},uid=1000,gid=1000 \
    make .gitlab-bundle

# Install gitlab npm dependencies
#
COPY --chown=gdk:gdk package.json yarn.lock ./gitlab/
COPY --chown=gdk:gdk scripts/frontend/postinstall.js ./gitlab/scripts/frontend/postinstall.js
RUN make .gitlab-yarn && yarn cache clean

# Copy code
COPY --chown=gdk:gdk ./ ./gitlab/
COPY --chown=gdk:gdk qa/gdk/entrypoint ../

# Create missing pids folder and sync compiled workhorse
RUN mkdir -p gitlab/tmp/pids \
    && rsync -a --remove-source-files workhorse/ gitlab/workhorse/

# Set up GDK
RUN SKIP_WORKHORSE_SETUP=true SKIP_GITLAB_SHELL_SETUP=true SKIP_GITALY_SETUP=true \
    make redis/redis.conf all \
    && gdk kill

ENTRYPOINT [ "/home/gdk/entrypoint" ]
CMD [ "gdk", "tail" ]

HEALTHCHECK --interval=10s --timeout=1s --start-period=5s --retries=17 \
    CMD curl --fail http://0.0.0.0:3000/users/sign_in || exit 1

EXPOSE 3000
